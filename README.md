#### Сборка проекта:
```bash
python setup.py bdist_wheel
```

#### Запуск тестов:
```bash
pip install -r requirements/development.txt
pytest tests --cov package
```
#### .gitlab-ci.yml

В файле содержится 4 стэйджа:
- *build*
- *testing*
- *docker*
- *kuber*  
На всех 4 стэйджах используется локальный gitlab-runner,
установленный на iMac macOS 10.13.6.  
`executor:Shell`  
`tag:[ci]`


1.  В стэйдже **build**  выполняется команда:
```bash
python3 setup.py bdist_wheel
```
Созданные директории для след стэйджа:
- *build*
- *dist*
- *package*
- *package.egg-info*  

2.  В стэйдже **testing**  выполняется команда:
```bash
- pip3 install -r requirements/development.txt
- pytest tests --cov package
```
Используется зависимость наличия директории *build project*

3.  В стэйдже **docker** используется команда для использования **docker deamon**
внутри **minikube** и запускаем билд **Dockerfile**.  
Добавляется текущее время в качестве тэга
```bash
- date +%Y%m%d%H%M > times.txt
- eval $(minikube docker-env)
- docker build -t newone:$(cat times.txt) .
```

4.  В стэйдже **kuber** у нас уже заранее создан ресурс типа **Deployment**  
Описан в [devtest-deployment.yaml](devtest-deployment.yaml)  
Командой меняем шаблон модуля используя новый **Docker image** с новым тэгом:  
```bash
- kubectl set image deployment/devtest-deployment devtest=newone:$(cat times.txt) --record
```

#### devtest-deployment.yaml 

* Используется группа **apps/v1**  
* Тип **Deployment**  
* Заданы 2 контроллера репликации  
* Имя модуля(pod) **devtest**  
* Имя **docker image :** `newone`
* Порт для контейнера **8085**
* Выставлен запрет на **pull** новых **image** из **DockerHub**


Деплой создан с помощью команды: 
```bash
- kubectl create -f devtest-deployment.yaml --record
```

Создана служба для доступа к приложению в контейнере:  
ps. LoadBalancer в миникубе не выдает внешний ip
```bash
- kubectl expose rc devtest --type=LoadBalancer --name devtest-http
```

Доступ к приложению с помощью команды:
```bash
minikube service devtest-http
```